const { MongoClient } = require("mongodb");
const { Parser } = require('@json2csv/plainjs');
var fs = require("fs");
require('dotenv').config();

const uri = process.env.MONGODB_URI || "mongodb://localhost:27017/";
const dbName = process.env.DATABASE_NAME;


function exportCSV(opts, data) {
  try {
    console.log("Exporting...")
    const parser = new Parser(opts);
    const csv = parser.parse(data);
    // console.log(csv);
    fs.writeFile('output.csv', csv, (err) => {
      if (err) {
        console.error(err);
      } else {
        console.log('CSV File created.');
      }
    });
  } catch (err) {
    console.error(err);
  }
}


async function getSubscribes(client) {
  const opts = {
    fields: [
      {
        label: 'Campaign',
        value: 'campaign',
      },
      {
        label: 'Campaign Name',
        value: 'campaignName',
        defaultValue: ''
      },
      {
        label: 'Display Name',
        value: 'displayName',
        defaultValue: ''
      },
      {
        label: 'Email',
        value: 'email',
        defaultValue: ''
      }
    ]
  }

  const column = await client.db(dbName).collection("subscribes");
  const pipeline = [
    {
      $lookup: {
        from: "campaigns",   
        localField: "campaign",   
        foreignField: "_id",   
        as: "joinedData"     
      }
    },
    {
      $unwind: "$joinedData"
    },
    {
      $project: {
        "campaign": 1,
        "displayName": 1,
        "email": 1,
        "campaignName": "$joinedData.name", 
      }
    },
    {
      $limit: 1000
    },
    {
      $sort: {
        "createdAt": -1
      }
    }
  ]
  console.log("Querying...")
  const data = await column.aggregate(pipeline).toArray();
  // console.log(data)
  exportCSV(opts, data)
}


async function main() {
  console.log("Connect to MongoDB: ", uri)
  const client = new MongoClient(uri);

  try {
    // Connect to the MongoDB cluster
    await getSubscribes(client)
  } catch (e) {
    console.error(e);
  } finally {
    await client.close();
  }
}

main().catch(console.error);